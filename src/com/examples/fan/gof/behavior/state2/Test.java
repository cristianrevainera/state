package com.examples.fan.gof.behavior.state2;


import com.examples.fan.gof.behavior.state2.Fan;

public class Test {

    public static void main(String[] args) {

        Fan fan = new Fan();

        System.out.println(fan);

        fan.pullChain();

        System.out.println(fan);

        fan.pullChain();

        System.out.println(fan);

        fan.pullChain();

        System.out.println(fan);

        fan.pullChain();

        System.out.println(fan);

        /*
        Fan is off.
        off handleRequest code..
        Turning fan on low.
        Fan is low.
        low handleRequest code..
        Turning fan on med.
        Fan is med.
        med handleRequest code..
        Turning fan on hight.
        Fan is hight.
        high handleRequest code..
        Turning fan on off.
        Fan is off.
         */
    }



}
