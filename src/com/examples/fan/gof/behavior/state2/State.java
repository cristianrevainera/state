package com.examples.fan.gof.behavior.state2;

public abstract class State {

    public void handleRequest() {
        System.out.println("Shouldn't be able to get here.");
    }
}
