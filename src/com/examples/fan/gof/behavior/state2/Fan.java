package com.examples.fan.gof.behavior.state2;

public class Fan {

    private State fanOffState;
    private State fanLowState;
    private State fanMedState;
    private State fanhightState;

    private State state;

    public Fan() {
        fanOffState = new FanOffState();
        fanLowState = new FanLowState();
        fanMedState = new FanMedState();
        fanhightState = new FanHightState();

        state = fanOffState;
    }

    public void pullChain() {

        state.handleRequest();

        if (state instanceof FanOffState) {
            System.out.println("Turning fan on low.");
            state = getFanLowState();
        } else if (state instanceof FanLowState) {
            System.out.println("Turning fan on med.");
            state = getFanMedState();
        } else if (state instanceof FanMedState) {
            System.out.println("Turning fan on hight.");
            state = getFanhightState();
        } else  if (state instanceof FanHightState) {
            System.out.println("Turning fan on off.");
            state = getFanOffState();
        }
    }

    public String toString() {
        return state.toString();
    }

    public State getFanOffState() {
        return fanOffState;
    }

    public State getFanLowState() {
        return fanLowState;
    }

    public State getFanMedState() {
        return fanMedState;
    }

    public State getFanhightState() {
        return fanhightState;
    }

    public void setState(State state) {
        this.state = state;
    }

}
