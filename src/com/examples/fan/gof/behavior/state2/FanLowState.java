package com.examples.fan.gof.behavior.state2;

public class FanLowState extends State {

    @Override
    public void handleRequest() {
        System.out.println("low handleRequest code..");
    }

    public String toString() {
        return "Fan is low.";
    }
}
