package com.examples.fan.gof.behavior.state2;

public class FanMedState extends State {

    @Override
    public void handleRequest() {
        System.out.println("med handleRequest code..");
    }

    public String toString() {
        return "Fan is med.";
    }
}
