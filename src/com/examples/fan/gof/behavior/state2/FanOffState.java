package com.examples.fan.gof.behavior.state2;

public class FanOffState extends State {

    @Override
    public void handleRequest() {
        System.out.println("off handleRequest code..");
    }

    public String toString() {
        return "Fan is off.";
    }
}
