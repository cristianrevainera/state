package com.examples.fan.gof.behavior.state2;

public class FanHightState extends State {

    @Override
    public void handleRequest() {
        System.out.println("high handleRequest code..");
    }

    public String toString() {
        return "Fan is hight.";
    }
}
