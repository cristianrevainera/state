package com.examples.fan.gof.behavior.state1;


public class Test {

    public static void main(String[] args) {

        Fan fan = new Fan();

        System.out.println(fan);

        fan.pullChain();

        System.out.println(fan);

        fan.pullChain();

        System.out.println(fan);

        fan.pullChain();

        System.out.println(fan);

        fan.pullChain();

        System.out.println(fan);

        /*
        Fan is off.
        Turning fan on low.
        Fan is low.
        Turning fan on med.
        Fan is med.
        Turning fan on hight.
        Fan is hight.
        Turning fan on off.
        Fan is off.
         */
    }



}
