package com.examples.fan.gof.behavior.state1;

public class FanMedState extends State {

    private Fan fan;

    public FanMedState(Fan fan) {
        this.fan = fan;
    }

    @Override
    public void handleRequest() {
        System.out.println("Turning fan on hight.");
        fan.setState(fan.getFanHightState());
    }

    public String toString() {
        return "Fan is med.";
    }
}
