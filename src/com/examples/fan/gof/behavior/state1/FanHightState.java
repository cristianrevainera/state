package com.examples.fan.gof.behavior.state1;

public class FanHightState extends State {

    private Fan fan;

    public FanHightState(Fan fan) {
        this.fan = fan;
    }

    @Override
    public void handleRequest() {
        System.out.println("Turning fan on off.");
        fan.setState(fan.getFanOffState());
    }

    public String toString() {
        return "Fan is hight.";
    }
}
