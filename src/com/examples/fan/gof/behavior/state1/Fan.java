package com.examples.fan.gof.behavior.state1;

public class Fan {


    private State fanOffState;
    private State fanLowState;
    private State fanMedState;
    private State fanHightState;

    private State state;

    public Fan() {
        fanOffState = new FanOffState(this);
        fanLowState = new FanLowState(this);
        fanMedState = new FanMedState(this);
        fanHightState = new FanHightState(this);

        state = fanOffState;
    }

    public void pullChain() {
        state.handleRequest();
    }

    public State getFanOffState() {
        return fanOffState;
    }

    public State getFanLowState() {
        return fanLowState;
    }

    public State getFanMedState() {
        return fanMedState;
    }

    public State getFanHightState() {
        return fanHightState;
    }

    public void setState(State state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return state.toString();
    }
}
