package com.examples.bank.gof.behavior.state;

public interface BankWindowStatus {
    void serve(Customer customer);
}
