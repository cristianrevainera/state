package com.examples.bank.gof.behavior.state;

public class Closed implements BankWindowStatus {
    @Override
    public void serve(Customer customer) {
        System.out.println("the bank window is closed: " + customer.getFirstName());
    }
}
