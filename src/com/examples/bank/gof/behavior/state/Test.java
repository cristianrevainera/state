package com.examples.bank.gof.behavior.state;

public class Test {

    public static void main(String []args) {

        Customer homer = new Customer("Homer",  "Simpson", 36);
        Customer marge = new Customer("Marge", "Simpson", 34);
        Customer abraham = new Customer("Abraham",  "Simpson", 70);

        BankWindow bankWindow = new BankWindow();
        bankWindow.serve(homer);

        bankWindow.stop();
        bankWindow.serve(marge);
        bankWindow.serve(abraham);

        bankWindow.close();
        bankWindow.serve(abraham);

        /**
         * BankWindow is open
         serving customer: Homer
         * BankWindow is stop
         please wait 5 minutes: Marge
         serving customer: Abraham
         * BankWindow is close
         the bank window is closed
         */

    }
}
