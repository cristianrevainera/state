package com.examples.bank.gof.behavior.state;

public class Stopped implements BankWindowStatus {
    @Override
    public void serve(Customer customer) {
        if (customer.getAge()>65) {
            System.out.println("serving customer: " + customer.getFirstName());
        } else {
            System.out.println("please wait 5 minutes: " + customer.getFirstName());
        }
    }
}
