package com.examples.bank.gof.behavior.state;

public class BankWindow {
    // private String teller;
    private BankWindowStatus bankWindowStatus;

    public BankWindow() {
        System.out.println("* BankWindow is open");
        bankWindowStatus = new Opened();
    }

    public void open() {
        System.out.println("* BankWindow is open");
        bankWindowStatus = new Opened();
    }

    public void close() {
        System.out.println("* BankWindow is close");
        bankWindowStatus = new Closed();
    }

    public void stop() {
        System.out.println("* BankWindow is stop");
        bankWindowStatus = new Stopped();
    }

    public void serve(Customer customer) {
        bankWindowStatus.serve(customer);
    }
}
