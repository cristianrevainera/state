package com.examples.bank.gof.behavior.state;

public class Opened implements BankWindowStatus {

    @Override
    public void serve(Customer customer) {
        System.out.println("serving customer: " + customer.getFirstName());
    }
}
