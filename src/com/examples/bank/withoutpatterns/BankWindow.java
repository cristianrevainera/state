package com.examples.bank.withoutpatterns;

import com.examples.bank.gof.behavior.state.Customer;

public class BankWindow {


    private final static int CLOSED = 0;
    private final static int OPENED = 1;
    private final static int STOPPED = 2;

    private int status;

    public BankWindow() {
        System.out.println("* BankWindow is open");
        status = CLOSED;
    }

    public void serve(Customer customer) {
        if (status == OPENED) {
            System.out.println("serving customer: " + customer.getFirstName());
        } else if(status == CLOSED) {
            System.out.println("the bank window is closed: " + customer.getFirstName());
        } else if (status == STOPPED) {
            if (customer.getAge()>65) {
                System.out.println("serving customer: " + customer.getFirstName());
            } else {
                System.out.println("please wait 5 minutes: " + customer.getFirstName());
            }
        }
    }
}
